# Convert MKV file to MP4 file

## Description

Convert an [MKV](https://en.wikipedia.org/wiki/Matroska) file to [MP4](https://en.wikipedia.org/wiki/MP4_file_format). My goal is to make quick and dirty and available to use in Adobe Premiere Pro as New Project. Because Premiere Pro does not support MKV files as input.

As a note, the underlyings can also be used to make "standard" MP4 file that Adobe Premiere Pro can read properly without crashing. See [convert-mkv-to-mp4](../convert-mp4-to-mp4).

## Installation

No installations required.
Only tested on Windows 10 with Powershell 7.2.6

## Usage

```shell
./convert-mkv-2-mp4.ps1 <filename>
```

1. This will take as input a plain file name without it's extension (supposed to be `.mkv`).
2. This will :
	1. Create a file in the running directory of the script.
	2. The file will be of MP4 Format.


## Support

No support given. It's personal but public.

## Roadmap

* Add supports for an array of filenames as an input.

## Contributing

I don't see anyone intereseted in this repo. However, feel free to suggest your ideas.

## Authors and acknowledgment

Thanks for all the posts I read on StackOverflow / StackExchange and people figuring out solutions to unknown.

## License

[Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/)