# FFmpeg cheat sheet

## Description

A list of useful commands or scripts for the ffmpeg ([documentation](https://www.ffmpeg.org/ffmpeg.html)) command line tool written mostly for PowerShell users.

## Installation

No installations required.
Only tested on Windows 10 with Powershell 7.2.6

## Usage

Each script or commands will have his own folder with it's own `README.md` describing how to use it. It's should follow this template as well but no guarantee is given.

## Support

No support given. It's personal but public.

## Roadmap

No idea where this is going.

## Contributing

I don't see anyone intereseted in this repo. However, feel free to suggest your ideas.

## Authors and acknowledgment

Thanks for all the posts I read on StackOverflow / StackExchange and people figuring out solutions to unknown.

## License

[Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/)